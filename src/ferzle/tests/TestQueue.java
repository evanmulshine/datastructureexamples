package ferzle.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import ferzle.queue.ArrayListQueue;
import ferzle.queue.ArrayQueue;
import ferzle.queue.LinkedQueue;
import ferzle.queue.QueueInterface;

/**
 * A simple test program for a queue
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 * @modified Feb 2013 (Updated to use JUnit)
 */
public class TestQueue extends Object {
	
	@Test
	public void testArrayListqueue() {
		QueueInterface<String> queue = new ArrayListQueue<String>();
		testThisqueue(queue);

	}

	@Test
	public void testLinkedqueue() {
		QueueInterface<String> queue = new LinkedQueue<String>();
		testThisqueue(queue);

	}

	@Test
	public void testArrayqueue() {
		QueueInterface<String> queue = new ArrayQueue<String>(55);
		testThisqueue(queue);

	}

	public void testThisqueue(QueueInterface<String> queue) {

		// It should be empty to start.
		assertTrue(queue.isEmpty());

		// It should definitely not be full.
		assertFalse(queue.isFull());
		
		// Does it return null if you peek on an empty queue?
		// (Or does it crash instead?)
		assertNull(queue.peek());

		// Adding one element makes it neither full nor empty.
		queue.enqueue("blah");
		assertFalse(queue.isEmpty());
		assertFalse(queue.isFull());

		// Add two more elements
		queue.enqueue("foo");
		queue.enqueue("ferzle");

		// queue should not be full.
		assertFalse(queue.isFull());

		assertEquals("blah", queue.peek());
		assertEquals("blah", queue.dequeue());
		assertEquals("foo", queue.dequeue());
		assertEquals("ferzle", queue.dequeue());
		assertTrue(queue.isEmpty());

		// Now do longer tests
		for (int j = 0; j < 50; j++) {
			queue.enqueue(j + "");
		}
		for (int j = 0; j < 50; j++) {
			assertEquals(j + "", queue.dequeue());
		}
		for (int j = 51; j < 100; j++) {
			queue.enqueue(j + "");
		}
		for (int j = 51; j < 100; j++) {
			assertEquals(j + "", queue.dequeue());
		}

	}
}
